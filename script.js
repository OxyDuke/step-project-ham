
const tabsContainer = document.getElementById("tabs");
const tabsTitles = tabsContainer.querySelectorAll(".tabs-title");
const tabsContentItems = document.querySelectorAll("#tabsContent li");

tabsContainer.addEventListener("click", (e) => {
  tabsTitles.forEach((title) => {
    title.classList.remove("active");
  });

  if (e.target.classList.contains("tabs-title")) {
    e.target.classList.add("active");

    const content = e.target.dataset.content;

  
    tabsContentItems.forEach((item) => {
      item.classList.remove("active");
    });

    const contentItem = document.querySelector(`#tabsContent li[data-content="${content}"]`);
    if (contentItem) {
      contentItem.classList.add("active");
    }
  }
});

const workImages = [
  {image: './img/section_work/graphic design/graphic-design5.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design6.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design7.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design8.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design9.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design10.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design11.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design12.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/landing page/landing-page5.jpg', data: "landing-page", elementFilter: "Landing pages"},
  {image: './img/section_work/landing page/landing-page6.jpg', data: "landing-page", elementFilter: "Landing pages"},
  {image: './img/section_work/landing page/landing-page7.jpg', data: "landing-page", elementFilter: "Landing pages"},
  {image: './img/section_work/web design/web-design5.jpg', data: "web-design", elementFilter: "Web design"},
  {image: './img/section_work/web design/web-design6.jpg', data: "web-design", elementFilter: "Web design"},
  {image: './img/section_work/wordpress/wordpress5.jpg', data: "wordpress", elementFilter: "Wordpress"},
  {image: './img/section_work/wordpress/wordpress6.jpg', data: "wordpress", elementFilter: "Wordpress"},
  {image: './img/section_work/wordpress/wordpress7.jpg', data: "wordpress", elementFilter: "Wordpress"},
  {image: './img/section_work/wordpress/wordpress8.jpg', data: "wordpress", elementFilter: "Wordpress"},
  {image: './img/section_work/wordpress/wordpress9.jpg', data: "wordpress", elementFilter: "Wordpress"},
  {image: './img/section_work/wordpress/wordpress10.jpg', data: "wordpress", elementFilter: "Wordpress"},
  {image: './img/section_work/graphic design/graphic-design2.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design3.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design4.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design5.jpg', data: "graphic-design", elementFilter: "Graphic design"},
  {image: './img/section_work/graphic design/graphic-design6.jpg', data: "graphic-design", elementFilter: "Graphic design"},
]

function workFilter() {
  const category = document.querySelectorAll('.category');
  const workImages = document.querySelectorAll('.work');

  category.forEach(tab => {
    tab.addEventListener('click', () => {

      category.forEach(tab => tab.classList.remove('active'));
      tab.classList.add('active');

      const selectedCategory = tab.getAttribute('data-title');

      workImages.forEach(image => {
        const imageCategory = image.getAttribute('data-content');

        if (selectedCategory === 'all' || selectedCategory === imageCategory) {
          image.style.display = 'block';
        } else {
          image.style.display = 'none';
        }
      });
    });
  });
}
workFilter();
const containerWorks = document.querySelector(".works");
const loadBtn = document.querySelector("#loadBtn");
let index = 0; 
loadBtn.addEventListener("click", function(){
  for(let i = 0; i < 12; i++){
    const newContainer = document.createElement("div")
    newContainer.classList.add("work");
    containerWorks.append(newContainer);
    const newImg = document.createElement("img");
    const indexWorkImage = workImages[index];
    
    newContainer.innerHTML = `
    
      <div class="work-card">
        <a href="#"><i class="fas fa-link"></i></a>
        <a href="#"><i class="fas fa-search"></i></a>
        <h5 class="green">Creative design</h5>
        <p class="work-underTitle">${indexWorkImage.elementFilter}</p>
      </div>

    `

    newImg.src = indexWorkImage.image;
    newContainer.setAttribute("data-content", indexWorkImage.data);
    newContainer.append(newImg);
    index++;
  }
  if(index === 24){
    loadBtn.style.display = "none";
  }
  workFilter();
})

//Carousel
const slideWidth = document.querySelector(".person").offsetWidth;
const peopleContainer = document.querySelector(".people");
const imgButtons = document.querySelectorAll(".img-btn");
 
let activeIndex = 0;

function updateCarousel() {
  peopleContainer.style.left = `-${activeIndex * slideWidth}px`;
  imgButtons.forEach((btn, index) => {
    btn.classList.remove("active");
    if (index === activeIndex) {
      btn.classList.add("active");
    }
  });
}

document.querySelector("#carTabs").addEventListener("click", function (e) {
  if (e.target.classList.contains("ctrl-btn")) {
    
    if (e.target.id === "fBtn") {
      activeIndex = (activeIndex + 1) % imgButtons.length;
    } else if (e.target.id === "bBtn") {
      activeIndex = (activeIndex - 1 + imgButtons.length) % imgButtons.length;
    }
    updateCarousel();
  }
});

updateCarousel();

//Mansory
  // const masonryContainer = document.getElementById("imageContainer");

  // function loadMoreImages() {
  //   const spinner = document.getElementById("spinner");
  //   spinner.classList.add("loading");

  //   setTimeout(() => {
  //     spinner.classList.remove("loading");

  //     for (let i = 1; i <= 4; i++) {
  //       const image = imageForMasonry(i);
  //       masonryContainer.appendChild(image);
  //     }
  //   }, 3000);
  // }

  // function imageForMasonry(elementNumber) {
  //   const imageForAdding = document.createElement("img");
  //   imageForAdding.src = 'img\section_gallery';
  //   imageForAdding.alt = 'img\section_gallery\(n).png';
  //   return imageForAdding;
  // }

  // const loadMoreButton2 = document.getElementById("loadBtn2");
  // loadMoreButton2.addEventListener("click", loadMoreImages);
